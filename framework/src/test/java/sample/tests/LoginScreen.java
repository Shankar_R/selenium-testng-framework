package sample.tests;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import stf.drivers.GetWebDriver;

public class LoginScreen {

	WebDriver wDriver;
	
	
	@Parameters({"browser"})
	@BeforeClass
	public void beforeTest(String browser) {
		wDriver = new GetWebDriver(browser, "").get();
		
	}

	@AfterClass
	public void afterTest() {
		wDriver.quit();
	}

	@Test
	public void loginandvalidate() {
		System.out.println("loginandvalidate");
		wDriver.get("https://www.google.com/");
		System.out.println(wDriver.getTitle());
	}

	@Test
	public void logoutandvalidate() {
		System.out.println("logoutandvalidate");
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}
