package stf.mainrunner;



import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.testng.TestNG;
import org.testng.xml.XmlClass;
import org.testng.xml.XmlSuite;
import org.testng.xml.XmlSuite.ParallelMode;
import org.testng.xml.XmlTest;

import stf.common.Constants;
import stf.utilities.ExcelData;
import stf.utilities.RunProperties;
import stf.utilities.TestNGUtilities;



public class DriverScript {

	private static RunProperties curRunProps = new RunProperties();
	private static String drivertdpath = System.getProperty("user.dir");

	public static void main(String[] args) throws FileNotFoundException, IOException {
		drivertdpath += curRunProps.getProperty(Constants.RP_exceldriverdatapath);

		HashMap<String, HashMap<String, String>> driverData = ExcelData.getRunnableTestsData(drivertdpath,
				curRunProps.getProperty(Constants.RP_driverdatasheetname));
		String[] browsers = curRunProps.getPropertyValueArr(Constants.RP_Browser) ;
		
		TestNG curRunTestNG = new TestNG();

		if (curRunProps.getProperty(Constants.RP_runType).equalsIgnoreCase(Constants.RT_ModuleLevel)) {
			
			String[] modulesToRun = curRunProps.getProperty(Constants.RP_ModulesToRun).split(Constants.COMMA_SPLITTER);
			
			List<XmlSuite> curRunSuites = getRunnableSuites(driverData, browsers, curRunProps.getProperty(Constants.RP_SuiteName), modulesToRun);
			
			curRunTestNG.setXmlSuites(curRunSuites);
			
			curRunTestNG.run();
			
		
		} else if (curRunProps.getProperty(Constants.RP_runType).equalsIgnoreCase(Constants.RT_AllTest)){
			// running all tests
			
			
			List<XmlSuite> curRunSuites = new ArrayList<>();
			
			ArrayList<XmlClass> runableClasses = TestNGUtilities.getRunnableClasses(driverData);
			
			XmlSuite suite = getRunnableSuite(runableClasses,browsers,curRunProps.getProperty(Constants.RP_SuiteName));
			
			if(curRunProps.getProperty(Constants.RP_isParallelRun).equalsIgnoreCase("true")) {
				suite.setParallel(ParallelMode.TESTS);
			}
			
			suite.setName("mySuite");
			
			curRunSuites.add(suite);

			curRunTestNG.setXmlSuites(curRunSuites);
			
			curRunTestNG.run();
		}
	}

	public static XmlSuite getRunnableSuite(ArrayList<XmlClass> testClasses,String[] browsers,String testName) {
		
		XmlSuite suite = new XmlSuite();
		
		if(browsers != null && browsers.length > 0) {
			
			for (String string : browsers) {
				XmlTest test = new XmlTest(suite);
				test.addParameter(Constants.RP_Browser, string);
				test.setClasses(testClasses);
				test.setName(testName + "-browser-" + string);	
			}
		}else {
			XmlTest test = new XmlTest(suite);
			test.setClasses(testClasses);
			test.setName(testName);
		}
		return suite;
	}

	public static ArrayList<XmlSuite> getRunnableSuites(HashMap<String, HashMap<String, String>> driData,String[] browsers,String testName,String[] modules) {
		
		ArrayList<XmlSuite> curSuites = new ArrayList<XmlSuite>();
		
		for (String modName : modules) {
			
			XmlSuite curSuite = new XmlSuite();
			curSuite.setName(testName + "-" + modName);
			
			if(curRunProps.getProperty(Constants.RP_isParallelRun).equalsIgnoreCase("true")) {
				curSuite.setParallel(ParallelMode.TESTS);
			}
			
			if(browsers != null && browsers.length > 0) {
				
				for (String string : browsers) {
					XmlTest test = new XmlTest(curSuite);
					test.addParameter(Constants.RP_Browser, string);
					test.setClasses(TestNGUtilities.getRunnableClasses(driData, Constants.CN_MODULENAME, modName));
					test.setName(testName + "-module:" + modName + "-browser:" + string);	
				}
			}else {
				XmlTest test = new XmlTest(curSuite);
				test.setClasses(TestNGUtilities.getRunnableClasses(driData, Constants.CN_MODULENAME, modName));
				test.setName(testName);
			}
			curSuites.add(curSuite);
		}
		
		return curSuites;
	}
	
	
	public static RunProperties getCurRunProps() {
		return curRunProps;
	}
	
}