package stf.common;

import org.openqa.selenium.Platform;

public abstract class Constants {
	public final static int TESTDATA_HEADER_ROW = 0;
	public final static String CN_TESTCASEID = "testcaseid";
	public final static String CN_TESTCLASSES = "testclasses";
	public final static String CN_MODULENAME = "modulename";
	public final static String CN_TESTCASEDESCRIPTION = "testcasedescription";
	public final static String CN_RUNFLAG = "runflag";

	public final static String COMMA_SPLITTER = "\\,";
	
	public final static String TESTS_SPLITTER = "\\|";

	// Browser names
	public final static String B_CHROME = "chrome";
	public final static String B_FIREFOX = "firefox";
	public final static String B_IE = "ie";
	public final static String B_EDGE = "edge";

	// Platforms
	public static Platform getPlatform(String platformName) {

		if (platformName.toLowerCase().startsWith("mac")) {
			return Platform.MAC;
		} else if (platformName.toLowerCase().startsWith("linux")) {
			return Platform.LINUX;
		} else {
			return Platform.WINDOWS;
		}

	}

	// Run properties
	public final static String RP_hubURL = "hubURL";
	public final static String RP_runType = "runType";
	public final static String RP_isLocalRun = "isLocalRun";
	public final static String RP_isParallelRun = "isParallelRun";
	public final static String RP_exceldriverdatapath = "exceldriverdatapath";
	public final static String RP_driverdatasheetname = "driverdatasheetname";
	public final static String RP_generateJSONReport = "generateJSONReport";
	public final static String RP_generateHTMLReport = "generateHTMLReport";
	public final static String RP_reportBasePath = "reportBasePath";
	public final static String RP_Browser = "browser";
	public final static String RP_ModulesToRun = "modulesToRun";
	public final static String RP_SuiteName = "mainSuiteName";
	
	public final static String RP_deleteCockies = "cmn_deleteCockies";
	public final static String RP_implicitWaitSeconds = "cmn_implicitWaitSeconds";
	public final static String RP_pageLoadTimeWaitSeconds = "cmn_pageLoadTimeWaitSeconds";
	public final static String RP_webdriverWaitSeconds = "cmn_webdriverWaitSeconds";
	public final static String RP_maximize = "cmn_maximize";
	
	
	public final static String RT_AllTest = "alltest";
	public final static String RT_ModuleLevel = "module";
	
	
	

}
