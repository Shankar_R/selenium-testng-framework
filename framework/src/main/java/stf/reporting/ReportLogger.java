package stf.reporting;

public class ReportLogger {

	public static void Info(String Info) {
		System.out.println(Info);
	}
	public static void Info(String ExpectedResult,String ActualResult) {
		System.out.println("Expected: " + ExpectedResult + " <> Actual: " + ActualResult);
	}
	
	public static void Done(String Done) {
		System.out.println(Done);
	}
	public static void Done(String ExpectedResult,String ActualResult) {
		System.out.println("Expected: " + ExpectedResult + " <> Actual: " + ActualResult);
	}
	
	public static void Event(String Event) {
		System.out.println(Event);
	}
	public static void Event(String ExpectedResult,String ActualResult) {
		System.out.println("Expected: " + ExpectedResult + " <> Actual: " + ActualResult);
	}
	
	public static void Warning(String Warning) {
		System.out.println(Warning);
	}
	public static void Warning(String ExpectedResult,String ActualResult) {
		System.out.println("Expected: " + ExpectedResult + " <> Actual: " + ActualResult);
	}
	
	public static void Fail(String Fail) {
		System.out.println(Fail);
	}
	public static void Fail(String ExpectedResult,String ActualResult) {
		System.out.println("Expected: " + ExpectedResult + " <> Actual: " + ActualResult);
	}
	
	public static void Pass(String Pass) {
		System.out.println(Pass);
	}
	public static void Pass(String ExpectedResult,String ActualResult) {
		System.out.println("Expected: " + ExpectedResult + " <> Actual: " + ActualResult);
	}
	
}
