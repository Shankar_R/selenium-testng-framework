package stf.utilities;

import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import stf.reporting.ReportLogger;

@SuppressWarnings("serial")
public class RunProperties extends Properties {
	
	public RunProperties(){
		try {
			this.load(new FileInputStream(System.getProperty("user.dir") + "/testdata/run.properties"));
		}catch(Exception e) {
			ReportLogger.Fail("Run properties not found. more info: " + e.getMessage());
		}
	}
	
	public String[] getPropertyValueArr(String qualifier) {
		List<String> resultlist = new ArrayList<String>();
		
		this.stringPropertyNames().stream().filter(e -> e.startsWith(qualifier)).forEach(str -> resultlist.add(this.getProperty(str)));
		
		return (String[]) resultlist.toArray(new String[0]);
		
	}
}
