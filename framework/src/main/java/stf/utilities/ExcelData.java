package stf.utilities;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;

import javax.management.RuntimeErrorException;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import stf.common.Constants;
import stf.reporting.ReportLogger;

public class ExcelData {
	private String strExcelPath;
	private Workbook wb_Main;
	private Sheet s_Current;

	public ExcelData(String excelPath) throws RuntimeErrorException, FileNotFoundException, IOException {
		if (excelPath.endsWith(".xls")) {
			this.strExcelPath = excelPath;
			prepareExcelObject();
		} else {
			throw new RuntimeErrorException(null, "This suite only supports XLS file format");
		}

	}

	public static String[][] getTestData(String excelPath, String sheetName, String testcaseName) {
		Workbook testdatabook = null;
		Sheet testdatasheet = null;
		FileInputStream testdatapath = null;

		try {
			testdatapath = new FileInputStream(excelPath);
		} catch (Exception e) {
			ReportLogger.Fail(excelPath + " is not found. additional info - " + e.getMessage());
		}

		if (testdatapath != null) {

			if (excelPath.endsWith(".xls")) {
				try {
					testdatabook = new HSSFWorkbook(testdatapath);
				} catch (IOException e) {
					ReportLogger.Fail(excelPath + " is not available. additional info - " + e.getMessage());
				}
			} else if (excelPath.endsWith(".xlsx")) {
				try {
					testdatabook = new XSSFWorkbook(testdatapath);
				} catch (IOException e) {
					ReportLogger.Fail(excelPath + " is not available. additional info - " + e.getMessage());
				}
			}
		}

		if (testdatabook != null) {
			testdatasheet = testdatabook.getSheet(sheetName);

			if (testdatasheet != null) {

				int rowCount = testdatasheet.getLastRowNum() - testdatasheet.getFirstRowNum();
				int colNum = getColumnNumber(testdatasheet, Constants.CN_TESTCASEID);

				int rowIndex = -1;
				int colIndex = -1;

				int tcRowCount = 0;
				int tcColCount = 0;
				// Calculating current test case row and column count to create String array
				for (int r = Constants.TESTDATA_HEADER_ROW + 1; r <= rowCount; r++) {
					String curRowValue = getCellValueAsString(testdatasheet.getRow(r).getCell(colNum));

					if (testcaseName.equalsIgnoreCase(curRowValue)) {
						tcColCount = testdatasheet.getRow(r).getLastCellNum();
						tcRowCount++;
					}
				}

				String[][] testdata = new String[tcRowCount][tcColCount];
				for (int r = Constants.TESTDATA_HEADER_ROW + 1; r <= rowCount; r++) {
					String curRowValue = getCellValueAsString(testdatasheet.getRow(r).getCell(colNum));

					if (testcaseName.equalsIgnoreCase(curRowValue)) {
						rowIndex++;
						colIndex = -1;
						int colCount = testdatasheet.getRow(r).getLastCellNum();

						for (int c = 0; c < colCount; c++) {

							String colName = getCellValueAsString(
									testdatasheet.getRow(Constants.TESTDATA_HEADER_ROW).getCell(c));
							String colVal = "";
							try {
								colVal = getCellValueAsString(testdatasheet.getRow(r).getCell(c));
							} catch (Exception e) {
								ReportLogger.Info(testcaseName + " does not find matched column <" + colName
										+ "> in the test data sheet");
							}
							colIndex++;
							testdata[rowIndex][colIndex] = colVal;
						}
					}
				}
				if (testdata != null)
					return testdata;

			} else {
				ReportLogger.Fail(sheetName + " is not available in the excel path <" + excelPath + ">");
			}
		}
		return new String[0][0];
	}

	public static HashMap<String, HashMap<String, String>> getRunnableTestsData(String excelPath, String sheetName) {
		Workbook testdatabook = null;
		Sheet testdatasheet = null;
		FileInputStream testdatapath = null;

		try {
			testdatapath = new FileInputStream(excelPath);
		} catch (Exception e) {
			ReportLogger.Fail(excelPath + " is not found. additional info - " + e.getMessage());
		}

		if (testdatapath != null) {

			if (excelPath.endsWith(".xls")) {
				try {
					testdatabook = new HSSFWorkbook(testdatapath);
				} catch (IOException e) {
					ReportLogger.Fail(excelPath + " is not available. additional info - " + e.getMessage());
				}
			} else if (excelPath.endsWith(".xlsx")) {
				try {
					testdatabook = new XSSFWorkbook(testdatapath);
				} catch (IOException e) {
					ReportLogger.Fail(excelPath + " is not available. additional info - " + e.getMessage());
				}
			}
		}

		if (testdatabook != null) {
			testdatasheet = testdatabook.getSheet(sheetName);

			if (testdatasheet != null) {
				int rowCount = testdatasheet.getLastRowNum() - testdatasheet.getFirstRowNum();
				int colNum = getColumnNumber(testdatasheet, Constants.CN_TESTCASEID);

				HashMap<String, HashMap<String, String>> driverData = new HashMap<String, HashMap<String, String>>();
				
				for (int r = Constants.TESTDATA_HEADER_ROW + 1; r <= rowCount; r++) {
					String curRowValue = getCellValueAsString(testdatasheet.getRow(r).getCell(colNum));
					
					HashMap<String, String> curRowData = new HashMap<String,String>();
					int colCount = testdatasheet.getRow(r).getLastCellNum();

					for (int c = 0; c < colCount; c++) {

						String colName = getCellValueAsString(
								testdatasheet.getRow(Constants.TESTDATA_HEADER_ROW).getCell(c));
						String colVal = "";

						try {
							colVal = getCellValueAsString(testdatasheet.getRow(r).getCell(c));
						} catch (Exception e) {
							ReportLogger.Info(" Error occured while getting data");
						}
						curRowData.put(colName, colVal);
					}
					
					driverData.put(curRowValue, curRowData);
					
				}
				
				return driverData;
			}
		}

		return null;
	}

	private void prepareExcelObject() throws FileNotFoundException, IOException {
		this.setMainWorkbook(new HSSFWorkbook(new FileInputStream(this.strExcelPath)));
	}

	public Workbook getMainWorkbook() {
		return wb_Main;
	}

	private void setMainWorkbook(Workbook wb_Main) {
		this.wb_Main = wb_Main;
	}

	public void setCurrentSheet(String strSheetName) {
		setCurrentSheet(wb_Main.getSheet(strSheetName));
	}

	public Sheet getCurrentSheet() {
		return this.s_Current;
	}

	public void setCurrentSheet(Sheet sheetObject) {
		this.s_Current = sheetObject;
	}

	public Object getCurrentTestCaseDataRow(String strTestCaseID) {

		/*
		 * Sheet sheetObject = getCurrentSheet(); int rowCount =
		 * sheetObject.getLastRowNum() - sheetObject.getFirstRowNum(); int colNum =
		 * getColumnNumber(sheetObject, Constants.defaultTCColumnName); TestDataRow tdr
		 * = new TestDataRow(strTestCaseID);
		 * 
		 * for (int r = Constants.defaultRowNumber + 1; r <= rowCount; r++) { String
		 * curRowValue = getCellValueAsString(sheetObject.getRow(r).getCell(colNum));
		 * 
		 * if (strTestCaseID.equalsIgnoreCase(curRowValue)) { int colCount =
		 * sheetObject.getRow(r).getLastCellNum(); for (int c = 0; c < colCount; c++) {
		 * String colName =
		 * getCellValueAsString(sheetObject.getRow(Constants.defaultRowNumber).getCell(c
		 * )); String colVal = getCellValueAsString(sheetObject.getRow(r).getCell(c));
		 * tdr.setCellValues(new VCell(colName, colVal)); } } } return tdr;
		 */

		return null;
	}

	public static int getColumnNumber(Sheet sObject, String strColumnName) {

		for (int c = 0; c < sObject.getRow(Constants.TESTDATA_HEADER_ROW).getLastCellNum(); c++) {
			Cell curCellValue = sObject.getRow(Constants.TESTDATA_HEADER_ROW).getCell(c);
			String strCellValue = getCellValueAsString(curCellValue);
			if (strColumnName.equalsIgnoreCase(strCellValue)) {
				return c;
			}
		}

		return -1;
	}

	public static String getCellValueAsString(Cell cell) {
		String strCellValue = null;
		strCellValue = cell.getStringCellValue();
		return strCellValue;
	}

}
