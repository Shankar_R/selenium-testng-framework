package stf.utilities;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.testng.xml.XmlClass;
import org.testng.xml.XmlTest;

import stf.common.Constants;

public class TestNGUtilities {
	
	public static ArrayList<XmlTest> getRunnableTests(HashMap<String, HashMap<String, String>> driData) {

		if (driData != null) {
			ArrayList<XmlTest> testArrTests = new ArrayList<XmlTest>();
			for (String tc : driData.keySet()) {
				testArrTests.add(createTestXML(tc,driData.get(tc).get(Constants.CN_TESTCLASSES)));
			}
			return testArrTests;
		}
		return null;
	}

	public static XmlTest createTestXML(String testcaseid,String testclasses) {
		XmlTest test = new XmlTest();
		test.setClasses(getXmLClassesList(testclasses));
		test.setName(testcaseid);
		return test;
	}
	public static List<XmlClass> getXmLClassesList(String testclasses){
		
		return Stream.of(testclasses.split(Constants.TESTS_SPLITTER))
			.map(tcls -> new XmlClass(tcls))
			.collect(Collectors.toList());
	}

	public static XmlClass createClassXML(String testcaseid,String testclasses) {
		XmlClass test = new XmlClass();
		test.setName(testcaseid);
		return test;
	}
	
	public static ArrayList<XmlClass> getRunnableClasses(HashMap<String, HashMap<String, String>> driData) {
		return getClasses(driData, "", "");
	}
	public static ArrayList<XmlClass> getRunnableClasses(HashMap<String, HashMap<String, String>> driData,String colFilter,String colValue) {
		return getClasses(driData, colFilter, colValue);
	}
	
	private static ArrayList<XmlClass> getClasses(HashMap<String, HashMap<String, String>> driData,String colFilter,String colValue) {
		
		ArrayList<XmlClass> testArrClasses = new ArrayList<XmlClass>();
		if (driData != null) {
			
			for (String tc : driData.keySet()) {
				if((colFilter != null && colFilter.isEmpty())  ? true : driData.get(tc).get(colFilter).equalsIgnoreCase(colValue)) {
					testArrClasses.addAll(getXmLClassesList(driData.get(tc).get(Constants.CN_TESTCLASSES)));
				}
			}
			return testArrClasses;
		}
		return testArrClasses;
	}

}
