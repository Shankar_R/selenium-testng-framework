package stf.drivers;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import stf.common.Constants;
import stf.mainrunner.DriverScript;
import stf.reporting.ReportLogger;

/*
 * This class reads the property file to get Hub URL and based on the constructor parameters it will create 
 * an instance of remote web driver
 */
public class GetWebDriver {
	WebDriver wDriver;
	WebDriverWait wDriverWait;

	public GetWebDriver(String browserName, String platformName) {

		if (DriverScript.getCurRunProps().getProperty(Constants.RP_isLocalRun).equalsIgnoreCase("true")) {
			
			if(browserName.equalsIgnoreCase(Constants.B_CHROME)) {
				wDriver = getChromeDriver();
			} else if(browserName.equalsIgnoreCase(Constants.B_FIREFOX)) {
				wDriver = getFireFoxDriver();
			}
			
		} else {
			DesiredCapabilities dCap = new DesiredCapabilities();
			dCap.setBrowserName(browserName);
			dCap.setPlatform(Constants.getPlatform(platformName));

			try {
				wDriver = new RemoteWebDriver(new URL(DriverScript.getCurRunProps().getProperty(Constants.RP_hubURL)), dCap);
			} catch (MalformedURLException e) {
				ReportLogger.Fail(e.getMessage());
			}
		}
		//Driver setup
		if(DriverScript.getCurRunProps().getProperty(Constants.RP_maximize).equalsIgnoreCase("true")) {
			wDriver.manage().window().maximize();
		}
		if(DriverScript.getCurRunProps().getProperty(Constants.RP_deleteCockies).equalsIgnoreCase("true")) {
			wDriver.manage().deleteAllCookies();
		}
		
		wDriver.manage().timeouts().implicitlyWait(Integer.parseInt(DriverScript.getCurRunProps().getProperty(Constants.RP_implicitWaitSeconds,"20")), TimeUnit.SECONDS);
				
		wDriver.manage().timeouts().pageLoadTimeout(Integer.parseInt(DriverScript.getCurRunProps().getProperty(Constants.RP_pageLoadTimeWaitSeconds,"20")), TimeUnit.SECONDS);
		
		wDriverWait = new WebDriverWait(wDriver, Integer.parseInt(DriverScript.getCurRunProps().getProperty(Constants.RP_webdriverWaitSeconds,"20")));
		
	}

	public WebDriver get() {
		return wDriver;
	}
	public WebDriverWait getWebDriverWait() {
		return wDriverWait;
	}

	private WebDriver getChromeDriver() {
		System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir") + "/drivers/chromedriver.exe");
		WebDriver chromeDrv = new ChromeDriver();
		return chromeDrv;
	}

	private WebDriver getFireFoxDriver() {
		System.setProperty("webdriver.gecko.driver", System.getProperty("user.dir") + "/drivers/geckodriver.exe");
		WebDriver chromeDrv = new FirefoxDriver();
		return chromeDrv;
	}
}
